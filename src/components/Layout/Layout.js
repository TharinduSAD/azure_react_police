import React from "react";

import Header from "../Header/Header";

export default props => {
    return (
      <div className="user-wrapper">
        <Header />
        <div className="user-site-pannel">{props.children}</div>
      </div>
    );
  }



