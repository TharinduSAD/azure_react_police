import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MailIcon from "@material-ui/icons/Mail";

import MenuIcon from "@material-ui/icons/Menu";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import Divider from "@material-ui/core/Divider";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Link from "@material-ui/core/Link";

import "./Header.css";

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1
  },
  root: {
    backgroundColor: "white",
    color: "#a5a5a5",
    position: "fixed"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    display: "flex",
    lineHeight: "19.99px",
    [theme.breakpoints.up("md")]: {
      fontSize: "20px",
      lineHeight: "19.99px"
    },
    [theme.breakpoints.down("md")]: {
      fontSize: "20px",
      lineHeight: "19.99px"
    }
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: 200
    }
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex"
    }
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  list: {
    width: 250
  },
  fullList: {
    width: "auto"
  },
  signIn: {
    fontSize: "16px"
  },
  dofine: {
    marginLeft: 8,
    marginRight: 10,
    marginTop: 1,
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  link: {
    color: "#a5a5a5",
    textDecoration: "none"
  }
}));

function PrimarySearchAppBar() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    left: false
  });

  const toggleDrawer = (side, open) => event => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List>
        {["Inbox", "Starred", "Send email", "Drafts"].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        {["All mail", "Trash", "Spam"].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <div className={classes.grow}>
      <AppBar position="static" classes={{ root: classes.root }}>
        <Toolbar>
          <img
            src={require("../../assets/img/favicon.png")}
            className="logo"
            alt="logo"
          />
          <Typography className={classes.title} variant="h6" noWrap>
            Trafficfine
          </Typography>
          <Typography className={classes.dofine} noWrap>
            Do fine
          </Typography>
          <Typography className={classes.dofine} noWrap>
            Enforced fines
          </Typography>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <IconButton color="inherit">
              <ExitToAppIcon />
              <Typography className={classes.signIn} noWrap>
                <Link href="signin" className={classes.link}>
                  Sign In
                </Link>
              </Typography>
            </IconButton>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-haspopup="true"
              onClick={toggleDrawer("left", true)}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer open={state.left} onClose={toggleDrawer("left", false)}>
        {sideList("left")}
      </Drawer>
    </div>
  );
}

export default PrimarySearchAppBar;
