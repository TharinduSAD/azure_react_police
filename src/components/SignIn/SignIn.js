import React, { Component } from "react";
import { connect } from "react-redux";
import { Card } from "primereact/card";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import { Message } from "primereact/message";
import { Password } from "primereact/password";
import Grid from "@material-ui/core/Grid";
import { Redirect } from "react-router-dom";

import { loginActions } from "../../actions";

import "./SignIn.css";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      usernameError: false,
      passwordError: false,
      admin: ""
    };

    this.logIn = this.logIn.bind(this);
  }

  componentDidMount() {
    document.body.classList.add("background-color");
  }

  componentWillUnmount() {
    document.body.classList.replace("background-color", "no-color");
  }

  logIn() {
    if (this.state.username && this.state.password) {
      this.props.doLogin({
        username: this.state.username,
        pass: this.state.password
      });

      const user = JSON.parse(localStorage.getItem("user"));

      if (localStorage.getItem("jwt") && user.type === "Admin") {
        this.setState({
          user: "Admin"
        });
      }
    }

    if (!this.state.username) {
      this.setState({
        usernameError: true
      });
    }

    if (!this.state.password) {
      this.setState({
        passwordError: true
      });
    }
  }

  handleUsername(event) {
    this.setState({ username: event.target.value });
    if (this.state.username) {
      this.setState({
        usernameError: false
      });
    }
  }

  handlePassword(event) {
    this.setState({ password: event.target.value });
    if (this.state.password) {
      this.setState({
        passwordError: false
      });
    }
  }

  renderRedirect = () => {
    if (this.state.user==="Admin") {
      return <Redirect to="/admin/dashboard" />;
    }

    if(this.state.user===""){
      
    }
  };

  render() {
    const header = <div className="card-header1">SL Police</div>;
    const footer = <div className="card-footer1">Forget password?</div>;
    return (
      <div>
        {this.renderRedirect()}
        <div className="p-grid p-fluid p-dir-col">
          <Grid container>
            <Grid item xl={4} lg={4} md={4} sm={3} xs={12} />
            <Grid item xl={4} lg={4} md={4} sm={6} xs={12}>
              <div className="content-section implementation">
                <Card
                  title="Sign In"
                  style={{ width: "360px" }}
                  header={header}
                  footer={footer}
                >
                  <div className="p-inputgroup username input">
                    <span className="p-inputgroup-addon">
                      <i className="pi pi-user" />
                    </span>
                    <InputText
                      placeholder="Username"
                      keyfilter="email"
                      value={this.state.username}
                      onChange={this.handleUsername.bind(this)}
                      className={this.state.usernameError ? "p-error" : null}
                    />
                  </div>
                  {!this.state.usernameError ? null : (
                    <Message
                      severity="error"
                      text="Field is required"
                      className="error-msg"
                    />
                  )}

                  <div className="p-inputgroup input">
                    <span className="p-inputgroup-addon">
                      <i className="pi pi-lock" />
                    </span>
                    <Password
                      placeholder="password"
                      feedback={false}
                      value={this.state.password}
                      onChange={this.handlePassword.bind(this)}
                      className={this.state.passwordError ? "p-error" : null}
                    />
                  </div>
                  {!this.state.passwordError ? null : (
                    <Message
                      severity="error"
                      text="Field is required"
                      className="error-msg"
                    />
                  )}

                  <Button
                    className="submit-button"
                    label="Sign In"
                    icon="pi pi-sign-in"
                    iconPos="right"
                    tooltip="Click to Sign in"
                    onClick={this.logIn}
                  />
                </Card>
              </div>
            </Grid>
          </Grid>
          {/* <div className="p-col-4 p-lg-4" />
          <div className="p-col-4 p-lg-4"> */}

          {/* </div> */}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    login: state.login
  };
}

export default connect(
  mapStateToProps,
  loginActions
)(SignIn);
