export default {
  items: [
    {
      name: "Dashboard",
      url: "/admin/dashboard",
      icon: "icon-speedometer"
    },
    {
      name: "Administrators",
      url: "/base",
      icon: "icon-key",
      children: [
        {
          name: "Add Administrator",
          url: "/admin/addadmin",
          icon: "icon-key"
        },
        {
          name: "All Administrators",
          url: "/admin/administratorlist",
          icon: "icon-key"
        }
      ]
    },
    {
      name: "Officers",
      url: "/buttons",
      icon: "icon-home",
      children: [
        {
          name: "Add Officer",
          url: "/admin/Officeradd",
          icon: "icon-home"
        },
        {
          name: "All officers",
          url: "/buttons/button-dropdowns",
          icon: "icon-home"
        }
      ]
    },
    {
      name: "Offences",
      url: "/icons",
      icon: "icon-speech",
      children: [
        {
          name: "Add Offence",
          url: "/icons/coreui-icons",
          icon: "icon-speech"
        },
        {
          name: "Offences",
          url: "/icons/flags",
          icon: "icon-speech"
        }
      ]
    }
  ]
};
