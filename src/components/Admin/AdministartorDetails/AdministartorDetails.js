import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { userActions } from "../../../actions";
import { Card, CardBody, Col, Row, Table, FormGroup } from "reactstrap";
import "./AdministratorDetails.css";
import { Link } from "react-router-dom";

class AdministratorDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      administrator: []
    };
  }
  componentDidMount() {
    console.log(this.props.match.params);
    this.props.getAdminDetails({ identity: this.props.match.params });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.administrator !== prevState.administrator) {
      return { administrator: nextProps.administrator };
    }
  }

  render() {
    const { administrator } = this.state;
    var administratorDetails = JSON.parse(JSON.stringify(administrator));
    console.log(Object.prototype.toString.call(administratorDetails));

    return (
      <React.Fragment>
        <Row>
          <Col lg="1" />
          <Col lg="10">
            <Card>
              <div className="administrator-details-title">
                {administratorDetails["firstName"] &&
                  administratorDetails["firstName"].toUpperCase()}{" "}
                {administratorDetails["lastName"] &&
                  administratorDetails["lastName"].toUpperCase()}
              </div>
              <div className="administrator-details">
                <Row>
                  <Col lg={3}>
                    <div className="administrator-details-label">
                      Registration No:{" "}
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="administrator-details-value">
                      {administratorDetails["regNo"] &&
                        administratorDetails["regNo"]}
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={3}>
                    <div className="administrator-details-label">
                      Police Station:{" "}
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="administrator-details-value">
                      {administratorDetails["station"] &&
                        administratorDetails["station"]}
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={3}>
                    <div className="administrator-details-label">Status: </div>
                  </Col>
                  <Col lg={3}>
                    <div className="administrator-details-value">
                      {administratorDetails["status"] &&
                        administratorDetails["status"]}
                    </div>
                  </Col>
                </Row>
              </div>

              <hr className="administrator-details-hr"></hr>
              <div className="administrator-details-offence-panel">
                <Row>
                  <Col lg={3}>
                    <div className="administrator-details-label">
                      No of offences:{" "}
                    </div>
                  </Col>
                  <Col lg={3}>
                    <div className="administrator-details-value">
                      {administratorDetails["offenceCount"] &&
                        administratorDetails["offenceCount"]}
                    </div>
                  </Col>
                </Row>
              </div>
              <Table striped className="fine-table" responsive>
                <thead className="fine-table-header">
                  <tr>
                    <th style={{ textAlign: "center" }}>Fine ID</th>
                    <th style={{ textAlign: "center" }}>Driver ID</th>
                    <th style={{ textAlign: "center" }}>Location</th>
                    <th style={{ textAlign: "center" }}>Date</th>
                    <th style={{ textAlign: "center" }}>Vehicle No</th>
                    <th style={{ textAlign: "center" }}>Status</th>
                    <th style={{ textAlign: "center" }}>View</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>
                      <Link to="/officer/singleFine">
                        <i class="fa fa-eye view-icon" />
                      </Link>
                    </td>
                  </tr>
                  <tr>
                    <th>2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>
                      <i class="fa fa-eye view-icon" />
                    </td>
                  </tr>
                  <tr>
                    <th>3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>@twitter</td>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>
                      <i class="fa fa-eye view-icon" />
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Card>
          </Col>
          <Col lg="1" />
        </Row>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  console.log(state.user.administrator);
  return {
    administrator: state.user.administrator
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    userActions
  )(AdministratorDetails)
);
