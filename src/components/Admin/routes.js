import React from "react";
import AdministratorDetails from './AdministartorDetails/AdministartorDetails'

const Dashboard = React.lazy(() => import("./Dashboard/Dashboard"));
const Addadmin = React.lazy(() => import("./Addadmin/Addadmin"));
const Administratorlist = React.lazy(() =>
  import("./Administratorlist/Administratorlist")
);

const Officeradd = React.lazy(() =>import("./Officeradd/Officeradd"));



const routes = [
  { path: "/admin/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/admin/addadmin", name: "Add Administrator", component: Addadmin },
  {
    path: "/admin/administratorlist",
    name: "Addministrators",
    component: Administratorlist
  },{
    path: "/admin/AdministratorDetails/:id",
    name: "AdministratorDetails",
    component: AdministratorDetails
  },
  {
    path:"/admin/Officeradd",
    name: "OfficerAdd",
    component: Officeradd
  }
];

export default routes;
