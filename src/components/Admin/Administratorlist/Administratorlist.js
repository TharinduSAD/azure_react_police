import React, { Component } from "react";
import { Card, CardBody, Table } from "reactstrap";
import { connect } from "react-redux";
import { userActions } from "../../../actions";
import { Button } from "primereact/button";
import { Link } from "react-router-dom";

import "./Administratorlist.css";

class Administratorlist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      administrators: []
    };

    this.renderAdministrator = this.renderAdministrator.bind(this);
  }
  componentDidMount() {
    this.props.getAdministrators();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.administrators !== prevState.administrators) {
      return { administrators: nextProps.administrators };
    }
  }

  renderAdministrator() {
    var administrators = this.state.administrators;
    if (Object.keys(administrators) !== 0) {
      return Object.values(administrators).map(el => (
        <React.Fragment>
          <tr>
            <td>{el.user_id}</td>
            <td>{el.first_name}</td>
            <td>{el.last_name}</td>
            <td>{el.organization}</td>
            <td>{el.admin_level}</td>
            <td>{el.identity_key}</td>
            <td align="center">
              <Link to={`/admin/AdministratorDetails/${el.identity_key}`}>
                <Button
                  className="p-button-raised p-button-info admin-list-view-more"
                  label=""
                  icon="pi pi-eye"
                  tooltip="Click to view more"
                />
              </Link>
            </td>
          </tr>
        </React.Fragment>
      ));
    }
  }

  render() {
    return (
      <div>
        <Card>
          <CardBody>
            <Table hover bordered striped responsive size="sm">
              <thead className="administrator-list-table-header">
                <tr>
                  <th>user_id</th>
                  <th>first_name</th>
                  <th>last_name</th>
                  <th>organization</th>
                  <th>admin_level</th>
                  <th>identity_key</th>
                  <th />
                </tr>
              </thead>
              <tbody>{this.renderAdministrator()}</tbody>
            </Table>
          </CardBody>
        </Card>
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log(state);
  return {
    administrators: state.user.administrators
  };
}

export default connect(
  mapStateToProps,
  userActions
)(Administratorlist);
