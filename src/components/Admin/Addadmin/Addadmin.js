import React, { Component } from "react";
import { connect } from "react-redux";
import { Card } from "primereact/card";
import Grid from "@material-ui/core/Grid";
import { InputText } from "primereact/inputtext";
import { Password } from "primereact/password";
import { Button } from "primereact/button";
import { Message } from "primereact/message";
import "./Addadmin.css";
import { userActions } from "../../../actions";
import { Growl } from "primereact/growl";

class Addadmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      identity: "",
      first_name: "",
      last_name: "",
      organization: "",
      admin_level: "level 1",
      username: "",
      password: "",
      identity_key: "",
      station: "",
      identityError: null,
      identityKeyError: null,
      firstNameError: null,
      lastNameError: null,
      organizationError: null,
      usernameError: null,
      passwordError: null,
      stationError: null,
      user: null,
      usernameExits: null,
      userIdentityExits: null,
      userIdentityKeyExits: null,
      userIdentityKey: null
    };

    this.addAdministrator = this.addAdministrator.bind(this);
    this.clearForm = this.clearForm.bind(this);
    this.showSuccess = this.showSuccess.bind(this);
    this.showError = this.showError.bind(this);
  }

  showSuccess() {
    this.growl.show({
      severity: "success",
      summary: "Success Message",
      detail: "Successfully added administrator"
    });
  }

  showError() {
    this.growl.show({
      severity: "error",
      summary: "Error Message",
      detail: "Something went wrong"
    });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      nextProps.user &&
      Object.keys(nextProps.user).length !== 0 &&
      nextProps.user !== prevState.user
    ) {
      return {
        usernameExits: true
      };
    }

    if (
      nextProps.userIdentityKey &&
      Object.keys(nextProps.userIdentityKey).length !== 0 &&
      nextProps.userIdentityKey !== prevState.userIdentityKey
    ) {
      return {
        userIdentityKeyExits: true
      };
    } else {
      return {
        user: "",
        usernameExits: false,
        userIdentityKeyExits: false
      };
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.officer && prevProps.officer !== this.props.officer) {
      this.showSuccess();
    }

    if (
      Object.keys(this.props.error).length !== 0 &&
      prevProps.error !== this.props.error
    ) {
      this.showError();
    }
  }

  addAdministrator() {
    const {
      identity,
      first_name,
      last_name,
      organization,
      admin_level,
      username,
      password,
      identity_key,
      station
    } = this.state;
    if (
      (identity,
      identity_key,
      first_name,
      last_name,
      organization,
      admin_level,
      username,
      password,
      station)
    ) {
      this.props.addAdmin({
        identity: identity,
        identity_key: identity_key,
        first_name: first_name,
        last_name: last_name,
        organization: organization,
        admin_level: admin_level,
        username: username,
        password: password,
        station: station
      });
    }

    if (!identity) {
      this.setState({ identityError: true });
    }

    if (!first_name) {
      this.setState({ firstNameError: true });
    }

    if (!last_name) {
      this.setState({ lastNameError: true });
    }

    if (!organization) {
      this.setState({ organizationError: true });
    }

    if (!username) {
      this.setState({ usernameError: true });
    }

    if (!password) {
      this.setState({ passwordError: true });
    }

    if (!identity_key) {
      this.setState({ identityKeyError: true });
    }

    if (!station) {
      this.setState({ stationError: true });
    }
  }

  handleIdentity(event) {
    this.setState({ identity: event.target.value });
    if (this.state.identity) {
      this.setState({
        identityError: false
      });
    }
  }

  handleFirstname(event) {
    this.setState({ first_name: event.target.value });
    if (this.state.first_name) {
      this.setState({
        firstNameError: false
      });
    }
  }

  handleLastname(event) {
    this.setState({ last_name: event.target.value });
    if (this.state.last_name) {
      this.setState({
        lastNameError: false
      });
    }
  }

  handleOrganization(event) {
    this.setState({ organization: event.target.value });
    if (this.state.organization) {
      this.setState({
        organizationError: false
      });
    }
  }

  async handleUsername(event) {
    await this.setState({ username: event.target.value });
    if (this.state.username) {
      this.setState({
        usernameError: false
      });

      this.props.getUserByUserName({
        username: this.state.username
      });
    }
  }

  handlePassword(event) {
    this.setState({ password: event.target.value });
    if (this.state.password) {
      this.setState({
        passwordError: false
      });
    }
  }

  async handleIdentityKey(event) {
    await this.setState({ identity_key: event.target.value });
    if (this.state.password) {
      this.setState({
        identityKeyError: false
      });

      this.props.getAdminByIdentityKey({
        identity: this.state.identity_key
      });
    }
  }

  handleStation(event) {
    this.setState({ station: event.target.value });
    if (this.state.station) {
      this.setState({
        stationError: false
      });
    }
  }

  clearForm() {
    this.setState({
      identity: "",
      identity_key: "",
      first_name: "",
      last_name: "",
      organization: "",
      admin_level: "",
      username: "",
      password: "",
      station: ""
    });
  }

  render() {
    return (
      <div>
        <Growl ref={el => (this.growl = el)} className="add-admin-alert" />
        <Grid container>
          <Grid item xl={2} lg={3} md={4} sm={3} xs={12} />
          <Grid item xl={8} lg={6} md={4} sm={6} xs={12}>
            <Card title="Add Administrator">
              <div className="add-admin-card-container">
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">
                    <i className="pi pi-key" />
                  </span>
                  <InputText
                    placeholder="Identity"
                    className="add-admin-input"
                    value={this.state.identity}
                    onChange={this.handleIdentity.bind(this)}
                  />
                </div>
                {!this.state.identityError ? null : (
                  <Message
                    severity="error"
                    text="Field is required"
                    className="error-msg"
                  />
                )}
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">
                    <i className="pi pi-tag" />
                  </span>
                  <InputText
                    placeholder="First Name"
                    className="add-admin-input"
                    value={this.state.first_name}
                    onChange={this.handleFirstname.bind(this)}
                  />
                </div>
                {!this.state.firstNameError ? null : (
                  <Message
                    severity="error"
                    text="Field is required"
                    className="error-msg"
                  />
                )}
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">
                    <i className="pi pi-tag" />
                  </span>
                  <InputText
                    placeholder="Last Name"
                    className="add-admin-input"
                    value={this.state.last_name}
                    onChange={this.handleLastname.bind(this)}
                  />
                </div>
                {!this.state.lastNameError ? null : (
                  <Message
                    severity="error"
                    text="Field is required"
                    className="error-msg"
                  />
                )}
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">
                    <i className="pi pi-home" />
                  </span>
                  <InputText
                    placeholder="Organization"
                    className="add-admin-input"
                    value={this.state.organization}
                    onChange={this.handleOrganization.bind(this)}
                  />
                </div>
                {!this.state.organizationError ? null : (
                  <Message
                    severity="error"
                    text="Field is required"
                    className="error-msg"
                  />
                )}
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">
                    <i className="pi pi-user" />
                  </span>
                  <InputText
                    placeholder="username"
                    className="add-admin-input"
                    value={this.state.username}
                    onChange={this.handleUsername.bind(this)}
                  />
                </div>
                {!this.state.usernameError ? null : (
                  <Message
                    severity="error"
                    text="Field is required"
                    className="error-msg"
                  />
                )}
                {!this.state.usernameExits ? null : (
                  <Message
                    severity="error"
                    text="Username Exits !!"
                    className="error-msg"
                  />
                )}
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">
                    <i className="pi pi-lock" />
                  </span>
                  <Password
                    placeholder="password"
                    className="add-admin-input"
                    value={this.state.password}
                    onChange={this.handlePassword.bind(this)}
                  />
                </div>
                {!this.state.passwordError ? null : (
                  <Message
                    severity="error"
                    text="Field is required"
                    className="error-msg"
                  />
                )}
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">
                    <i className="pi pi-globe" />
                  </span>
                  <InputText
                    placeholder="Reg No"
                    className="add-admin-input"
                    value={this.state.identity_key}
                    onChange={this.handleIdentityKey.bind(this)}
                  />
                </div>
                {!this.state.identityKeyError ? null : (
                  <Message
                    severity="error"
                    text="Field is required"
                    className="error-msg"
                  />
                )}
                {!this.state.userIdentityKeyExits ? null : (
                  <Message
                    severity="error"
                    text="Reg No already  Exits !!"
                    className="error-msg"
                  />
                )}
                <div className="p-inputgroup">
                  <span className="p-inputgroup-addon">
                    <i className="pi pi-map-marker" />
                  </span>
                  <InputText
                    placeholder="Station"
                    className="add-admin-input"
                    value={this.state.station}
                    onChange={this.handleStation.bind(this)}
                  />
                </div>
                {!this.state.stationError ? null : (
                  <Message
                    severity="error"
                    text="Field is required"
                    className="error-msg"
                  />
                )}
                <Button
                  className="p-button-raised p-button-success add-admin-submit"
                  label="Add"
                  icon="pi pi-user-plus"
                  iconPos="right"
                  tooltip="Click to Add administrator"
                  onClick={this.addAdministrator}
                />
                <Button
                  className="p-button-raised p-button-danger"
                  label="Clear"
                  icon="pi pi-times-circle"
                  iconPos="right"
                  tooltip="Click to Clear the form"
                  onClick={this.clearForm}
                />
              </div>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const officer = state.user.officer;
  const error = state.user.error;
  const user = state.user.user[0];
  const userIdentityKey = state.user.userIdentityKey;
  return {
    login: state.login,
    officer: officer,
    error: error,
    user: user,
    userIdentityKey: userIdentityKey
  };
}

export default connect(
  mapStateToProps,
  userActions
)(Addadmin);
