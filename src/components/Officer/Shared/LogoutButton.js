import React from "react";
import { Button } from "primereact/button";

function LogoutButton() {
  return (
    <Button label="LOG OUT" className="p-button-rounded p-button-secondary officer-logout-button" />
  );
}

export default LogoutButton;
