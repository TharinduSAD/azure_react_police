import React, { Component } from "react";
import { Row, Container, Col, Button } from "reactstrap";
import "./Profile.css";
import { Link } from "react-router-dom";

class Profile extends Component {
  render() {
    return (
      <div>
        <div className="driver-name">DILSHAN GAMAGE</div>
        <div className="driver-profile-container">
          <Container>
            <Row>
              <Col>
                <div className="driver-profile-label">First Name:</div>
              </Col>
              <Col>
                <div className="driver-profile-value">Dilshan</div>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="driver-profile-label">Last Name:</div>
              </Col>
              <Col>
                <div className="driver-profile-value">Gamage</div>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="driver-profile-label">Designation:</div>
              </Col>
              <Col>
                <div className="driver-profile-value">Police Constable</div>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="driver-profile-label">Offence Count:</div>
              </Col>
              <Col>
                <div className="driver-profile-value">24</div>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="driver-profile-label">Username:</div>
              </Col>
              <Col>
                {" "}
                <div className="driver-profile-value">
                  dilshanmunaweera@gmail.com
                </div>
              </Col>
            </Row>
            <Link to="/officer/changePassword">
              <Button className="driver-profile-button">CHANGE PASSWORD</Button>
            </Link>
          </Container>
        </div>
      </div>
    );
  }
}

export default Profile;
