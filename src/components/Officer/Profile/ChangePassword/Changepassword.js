import React,{Component} from 'react';
import "./ChangePassword.css"

class ChangePassword extends Component{
    render(){
        return(
            <div><div className="change-password-title" style={{ textAlign:'center'}}>Change Password</div>
            <form className='form change-password-form'>
                <table>
                    <tr><td  className='lables'><label>Old Pasword:</label></td>
                    <td><input type="password" name="oldpass"/> <br/></td></tr>

                    <tr><td  className='lables'><label>New Password:</label></td>
                    <td><input type="password" name="newpass"/> <br/></td></tr>

                    <tr><td  className='lables'><label>Confirm Password:</label></td>
                    <td><input type="password" name="confirmpass"/> <br/></td></tr>

                    <tr><td></td><td colspan="2"><input type="submit" value="UPDATE PASSWORD"/>  </td></tr>
                </table>
            </form></div>
        )
    }
}

export default ChangePassword;