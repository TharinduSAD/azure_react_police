import React,{Component} from 'react'
import { Table, Container, FormGroup } from "reactstrap";
import { Link } from "react-router-dom";
import "./DriverList.css";


class DriverList extends Component{
    render(){
        return(
            <div><div className="driver-title">DRIVER SEARCH</div>
            <Container className="driver-table-container">
              <Table striped className="driver-table" responsive>
                <thead className="driver-table-header">
                  <tr>
                    <th style={{ textAlign: "center" }}>Driver ID</th>
                    <th style={{ textAlign: "center" }}>Name</th>
                    <th style={{ textAlign: "center" }}>NIC</th>
                    <th style={{ textAlign: "center" }}>No of Offenses</th>
                    <th style={{ textAlign: "center" }}>Penalty Points</th>
                    <th style={{ textAlign: "center" }}>View</th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="fine-table-search-row" id="search-row">
                    <td style={{ textAlign: "center" }}>
                      <FormGroup>
                        <div class="form-group has-input">
                          <input
                            type="text"
                            className="from-control input-search"
                          />
                          <img
                            src={require("../../../assets/img/search.png")}
                            className="search-icon"
                          />
                        </div>
                      </FormGroup>
                    </td>
                    <td style={{ textAlign: "center" }}>
                      <FormGroup>
                        <div class="form-group has-input">
                          <input
                            type="text"
                            className="from-control input-search"
                            disabled
                          />
                          <img
                            src={require("../../../assets/img/search.png")}
                            className="search-icon"
                          />
                        </div>
                      </FormGroup>
                    </td>
                    <td style={{ textAlign: "center" }}>
                      <FormGroup>
                        <div class="form-group has-input">
                          <input
                            type="text"
                            className="from-control input-search"
                          />
                          <img
                            src={require("../../../assets/img/search.png")}
                            className="search-icon"
                          />
                        </div>
                      </FormGroup>
                    </td>
                    <td style={{ textAlign: "center" }}>
                      <FormGroup>
                        <div class="form-group has-input">
                          <input
                            type="text"
                            className="from-control input-search"
                            disabled
                          />
                          <img
                            src={require("../../../assets/img/search.png")}
                            className="search-icon"
                          />
                        </div>
                      </FormGroup>
                    </td>
                    <td style={{ textAlign: "center" }}>
                      <FormGroup>
                        <div class="form-group has-input">
                          <input
                            type="text"
                            className="from-control input-search"
                            disabled
                          />
                          <img
                            src={require("../../../assets/img/search.png")}
                            className="search-icon"
                          />
                        </div>
                      </FormGroup>
                    </td>
                  </tr>
                  <tr>
                    <th>1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                    <td>Mark</td>
                    <td>
                      <Link to="/officer/driverDetails">
                        <i class="fa fa-eye view-icon" />
                      </Link>
                    </td>
                  </tr>
                  <tr>
                    <th>2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                    <td>Mark</td>
                    <td>
                      <i class="fa fa-eye view-icon" />
                    </td>
                  </tr>
                  <tr>
                    <th>3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>@twitter</td>
                    <td>Mark</td>
                    <td>
                      <i class="fa fa-eye view-icon" />
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Container></div>
        )
    }
}

export default DriverList;
