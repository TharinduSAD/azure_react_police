import React, { Component } from "react";
import "./EnforcedFines.css";
import { Table, Container, FormGroup } from "reactstrap";
import { Link } from "react-router-dom";

class EnforcedFines extends Component {
  render() {
    return (
      <div>
        <div className="fine-title">FINE SEARCH</div>
        <Container className="fine-table-container">
          <Table striped className="fine-table" responsive>
            <thead className="fine-table-header">
              <tr>
                <th style={{ textAlign: "center" }}>Fine ID</th>
                <th style={{ textAlign: "center" }}>Driver ID</th>
                <th style={{ textAlign: "center" }}>Location</th>
                <th style={{ textAlign: "center" }}>Date</th>
                <th style={{ textAlign: "center" }}>Vehicle No</th>
                <th style={{ textAlign: "center" }}>Status</th>
                <th style={{ textAlign: "center" }}>View</th>
              </tr>
            </thead>
            <tbody>
              <tr className="fine-table-search-row" id="search-row">
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                        disabled
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                        disabled
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                        disabled
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
              </tr>
              <tr>
                <th>1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>
                  <Link to="/officer/singleFine">
                    <i class="fa fa-eye view-icon" />
                  </Link>
                </td>
              </tr>
              <tr>
                <th>2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>
                  <i class="fa fa-eye view-icon" />
                </td>
              </tr>
              <tr>
                <th>3</th>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>
                  <i class="fa fa-eye view-icon" />
                </td>
              </tr>
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default EnforcedFines;
