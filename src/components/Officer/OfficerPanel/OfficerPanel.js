import React, { Component } from "react";
import "./OfficerPanel.css";
import {
  Card,
  Col,
  Row,
  Container,
  Button,
  Form,
  FormGroup,
  Label,
  CardHeader,
  CardFooter,
  CardBody
} from "reactstrap";

class OfficerPanel extends Component {
  render() {
    return (
      <div>
        <div className="do-fine-title">DO FINE</div>
        <Container className>
          <Form className="do-fine-form">
            <Row>
              <Col lg="6">
                <FormGroup>
                  <Row>
                    <Label className="input-label">Fine ID</Label>
                  </Row>
                  <div class="form-group has-input">
                    <img
                      src={require("../../../assets/img/file-02.png")}
                      srcSet={`${require("../../../assets/img/file-02@2x.png")} 2x, ${require("../../../assets/img/file-02@3x.png")} 3x`}
                      className="input-icon"
                      alt="fineId-image"
                    />
                    <input
                      type="text"
                      class="from-control input"
                      placeholder="Fine ID"
                    />
                  </div>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Label className="input-label">Location</Label>
                  </Row>
                  <div class="form-group has-input">
                    <img
                      src={require("../../../assets/img/img-574524.png")}
                      srcSet={`${require("../../../assets/img/img-574524@2x.png")} 2x, ${require("../../../assets/img/img-574524@3x.png")} 3x`}
                      className="input-icon"
                    />
                    <input
                      type="text"
                      class="from-control input"
                      placeholder="Location"
                    />
                  </div>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Label className="input-label">Vehicle</Label>
                  </Row>
                  <div class="form-group has-input">
                    <img
                      src={require("../../../assets/img/img-347047.png")}
                      srcSet={`${require("../../../assets/img/img-347047@2x.png")} 2x, ${require("../../../assets/img/img-347047@3x.png")} 3x`}
                      className="input-icon"
                    />
                    <input
                      type="text"
                      class="from-control input"
                      placeholder="Vehicle"
                    />
                  </div>
                </FormGroup>
              </Col>
              <Col lg="6" className="second-section-do-fine">
                <FormGroup>
                  <Row>
                    <Label className="input-label">Driver ID</Label>
                  </Row>
                  <div class="form-group has-input">
                    <img
                      src={require("../../../assets/img/img-210318.png")}
                      srcSet={`${require("../../../assets/img/img-210318@2x.png")} 2x, ${require("../../../assets/img/img-210318@3x.png")} 3x`}
                      className="input-icon"
                    />
                    <input
                      type="text"
                      class="from-control input"
                      placeholder="Driver ID"
                    />
                  </div>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Label className="input-label">Date & Time</Label>
                  </Row>
                  <div class="form-group has-input">
                    <img
                      src={require("../../../assets/img/calendar-512.png")}
                      srcSet={`${require("../../../assets/img/calendar-512@2x.png")} 2x, ${require("../../../assets/img/calendar-512@3x.png")} 3x`}
                      className="input-icon"
                    />
                    <input type="datetime-local" class="from-control input" />
                  </div>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Label className="input-label">Vehicle Number</Label>
                  </Row>
                  <div class="form-group has-input">
                    <img
                      src={require("../../../assets/img/32245.png")}
                      srcSet={`${require("../../../assets/img/32245@2x.png")} 2x, ${require("../../../assets/img/32245@3x.png")} 3x`}
                      className="input-icon"
                    />
                    <input
                      type="text"
                      class="from-control input"
                      placeholder="Vehicle Number"
                    />
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <FormGroup>
              <Row>
                <Label className="input-label">Driver Details</Label>
              </Row>
              <textarea rows="4" cols="50" className="driver-details-input" defaultValue="Enter Driver Details">
                
              </textarea>
            </FormGroup>
            <FormGroup>
              <Card className="officer-panel-offence-card">
                <CardHeader className="officer-panel-offence-card-header">
                  <Row>
                    {" "}
                    <img
                      src={require("../../../assets/img/fine-icon.png")}
                      srcSet={`${require("../../../assets/img/fine-icon@2x.png")} 2x, ${require("../../../assets/img/fine-icon@3x.png")} 3x`}
                      alt="fine-icon"
                      className="officer-panel-offence-card-icon"
                    />
                    <p className="officer-panel-offence-card-title">Offences</p>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col lg={8}>
                      <FormGroup>
                        <select
                          className="form-control offence-select"
                          id="exampleFormControlSelect1"
                        >
                          <option>Select Offence</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                      </FormGroup>
                    </Col>
                    <Col lg={4}>
                      <Row>
                        <Col lg={6}>
                          <input
                            type="text"
                            class="selected-fine-amount"
                            disabled
                            placeholder="Rs.00.00"
                          />
                        </Col>
                        <Col lg={6}>
                          <Button
                            variant="outline-light"
                            className="offence-select-cancel-button"
                          >
                            <i class="fa fa-times" />
                          </Button>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  <hr className="offence-select-hr-line"></hr>
                  {/* <Button
                    className="add-offence-button"
                    variant="outline-light"
                  >
                    <Row>
                      <i class="fa fa-plus add-offence-button-icon" />
                      Add Offence
                    </Row>
                  </Button> */}
                </CardBody>
                <CardFooter>
                  {/* <Row>Total:</Row>
                  <Row>
                    <p className="officer-panel-offence-card-total">
                      Rs. 00.00
                    </p>
                  </Row> */}
                </CardFooter>
              </Card>
            </FormGroup>
          </Form>
        </Container>
      </div>
    );
  }
}

export default OfficerPanel;
