import React, { Component } from "react";
import { Card, Row, Col } from "reactstrap";
import "./OfficerHeader.css";
import { Link, withRouter } from "react-router-dom";

const LogoutButton = React.lazy(() => import("../Shared/LogoutButton"));

class OfficerHeader extends Component {
  render() {
    return (
      <div>
        <LogoutButton />
        <div className="site-navigation-panel">
          <Row>
            <Col lg="4" md="4" xs="3" />
            <Col lg="4" md="4" xs="6">
              <Row>
                <Col lg="3" md="3" xs="3">
                  <div className="officer-navigation-link-text">Do Fine</div>
                  <Link to="/officer/doFine">
                    <Card className="do-fine-tab">
                      <img
                        src={require("../../../assets/img/fine-icon.png")}
                        srcSet={`${require("../../../assets/img/fine-icon@2x.png")} 2x, ${require("../../../assets/img/fine-icon@3x.png")} 3x`}
                        className={`fine-icon ${this.props.location.pathname ===
                          "/officer/doFine" && "is-active"}`}
                        alt="fine-icon"
                      />
                    </Card>
                  </Link>
                </Col>
                <Col lg="3" md="3" xs="3">
                  <div className="officer-navigation-link-text">
                    Fine Search
                  </div>
                  <Link to="/officer/fineSearch">
                    <Card className="view-fine-tab">
                      <img
                        src={require("../../../assets/img/file.png")}
                        srcSet={`${require("../../../assets/img/file@2x.png")} 2x, ${require("../../../assets/img/file@3x.png")} 3x`}
                        className={`file-icon ${(this.props.location
                          .pathname === "/officer/fineSearch" ||
                          this.props.location.pathname ===
                            "/officer/singleFine") &&
                          "is-active"}`}
                        alt="file-icon"
                      />
                    </Card>
                  </Link>
                </Col>
                <Col lg="3" md="3" xs="3">
                  <div className="officer-navigation-link-text">Profile</div>
                  <Link to="/officer/profile">
                    <Card className="view-profile-tab">
                      <img
                        src={require("../../../assets/img/officer.png")}
                        srcSet={`${require("../../../assets/img/officer@2x.png")} 2x, ${require("../../../assets/img/officer@3x.png")} 3x`}
                        className={`officer-icon ${this.props.location
                          .pathname === "/officer/profile" && "is-active"}`}
                        alt="officer-icon"
                      />
                    </Card>
                  </Link>
                </Col>
                <Col lg="3" md="3" xs="3">
                  <div className="officer-navigation-link-text">
                    Find Driver
                  </div>
                  <Link to="/officer/findDriver">
                    <Card className="search-driver-tab">
                      <img
                        src={require("../../../assets/img/driver.png")}
                        srcSet={`${require("../../../assets/img/driver@2x.png")} 2x, ${require("../../../assets/img/driver@3x.png")} 3x`}
                        className={`driver-icon ${(this.props.location
                          .pathname === "/officer/findDriver" ||
                          this.props.location.pathname === "/officer/driverDetails") &&
                          "is-active"}`}
                        alt="driver-icon"
                      />
                    </Card>
                  </Link>
                </Col>
              </Row>
            </Col>
            <Col lg="4" md="4" xs="3" />
          </Row>
        </div>
      </div>
    );
  }
}

export default withRouter(OfficerHeader);
