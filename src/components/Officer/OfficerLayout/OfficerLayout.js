import React, { Component, Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import routes from "../officer_routes";
import "./OfficerLayout.css";
import { Container, Card, Row, Col } from "reactstrap";

const OfficerHeader = React.lazy(() =>
  import("../OfficerHeader/OfficerHeader")
);

class OfficerLayout extends Component {
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );

  componentDidMount() {
    document.body.classList.add("officer-layout-background");
  }

  componentWillUnmount() {
    document.body.classList.replace("background-color", "no-color");
  }

  render() {
    return (
      <div>
        <Container fluid>
          <div className="app-panel">
            <div className="police-app-header">
              <Suspense fallback={this.loading()}>
                <OfficerHeader />
              </Suspense>
            </div>
            <Row>
              <Col lg="1" />
              <Col lg="10">
                <Card className="police-app-panel-card">
                  {" "}
                  <Suspense fallback={this.loading()}>
                    <Switch>
                      {routes.map((route, idx) => {
                        return route.component ? (
                          <Route
                            key={idx}
                            path={route.path}
                            exact={route.exact}
                            name={route.name}
                            render={props => <route.component {...props} />}
                          />
                        ) : null;
                      })}
                      <Redirect from="/officer" to="/officer/doFine" />
                    </Switch>
                  </Suspense>{" "}
                </Card>
              </Col>
              <Col lg="1" />
            </Row>
          </div>
        </Container>
        {/* <footer className="police-app-footer">iiii</footer> */}
      </div>
    );
  }
}

export default OfficerLayout;
