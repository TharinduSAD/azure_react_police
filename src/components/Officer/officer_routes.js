import React from "react";

const OfficerPanel = React.lazy(() => import("./OfficerPanel/OfficerPanel"));
const EnforcedFines = React.lazy(() => import("./EnforcedFines/EnforcedFines"));
const DriverList = React.lazy(() => import("./DriverList/DriverList"));
const FineSearchSingle = React.lazy(() =>
  import("./FineSearchSingle/FineSearchSingle")
);

const DriverDetails = React.lazy(() => import("./DriverDetails/DriverDetails"))
const Profile = React.lazy(()=> import("./Profile/Profile"))
const ChangePassword = React.lazy(() => import("./Profile/ChangePassword/Changepassword"))

const routes = [
  { path: "/officer/doFine", name: "DoFine", component: OfficerPanel },
  {
    path: "/officer/fineSearch",
    name: "Fine Search",
    component: EnforcedFines
  },
  { path: "/officer/findDriver", name: "findDriver", component: DriverList },
  {
    path: "/officer/singleFine",
    name: "SingleFine",
    component: FineSearchSingle
  },
  {
    path: "/officer/driverDetails",
    name: "Driver Details",
    component: DriverDetails
  },
  {
    path: "/officer/profile",
    name: "Profile",
    component: Profile
  },
  {
    path: "/officer/changePassword",
    name: "Change Password",
    component: ChangePassword
  }
];

export default routes;
