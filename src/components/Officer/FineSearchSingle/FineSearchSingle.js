import React, { Component } from "react";
import { Container, Col, Row, Button,Card, CardBody, CardHeader, FormGroup, CardFooter } from "reactstrap";

import "./FineSearchSingle.css";

class FineSearchSingle extends Component {
  render() {
    return (
      <div>
        <div className="single-fine-title">F1</div>
        <div className="single-fine-container">
          <Container>
            <Row>
              <Col lg="6">
                <Row>
                  <Col>
                    <div className="single-fine-field-label">Driver ID:</div>{" "}
                  </Col>
                  <Col>
                    <div className="single-fine-field-value">942990014V</div>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <div className="single-fine-field-label">Location:</div>{" "}
                  </Col>
                  <Col>
                    <div className="single-fine-field-value">Colombo -07</div>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <div className="single-fine-field-label">Date & Time:</div>{" "}
                  </Col>
                  <Col>
                    <div className="single-fine-field-value">
                      10/12/2019 12:35 AM
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <div className="single-fine-field-label">Vehicle:</div>{" "}
                  </Col>
                  <Col>
                    <div className="single-fine-field-value">Car</div>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <div className="single-fine-field-label">
                      Vehicle Number:
                    </div>{" "}
                  </Col>
                  <Col>
                    <div className="single-fine-field-value">1233445667</div>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <div className="single-fine-field-label">Status:</div>{" "}
                  </Col>
                  <Col>
                    <div className="single-fine-field-value-danger">
                      Expired
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col lg="6">
                <div className="single-fine-field-label-second-col">
                  Driver Details :
                </div>{" "}
                <div className="card single-fine-field-value-second-col-card">
                  <Row>
                    <Col lg="6">
                      <div className="single-fine-second-col-field-label">
                        Name :
                      </div>
                    </Col>
                    <Col lg="6">
                      <div className="single-fine-second-col-field-value" >Tharindu</div>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg="6">
                      <div className="single-fine-second-col-field-label">
                        NIC :
                      </div>
                    </Col>
                    <Col lg="6">
                      <div className="single-fine-second-col-field-value" >yyyyyyy</div>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg="6">
                      <div className="single-fine-second-col-field-label">
                        Number of Offences :
                      </div>
                    </Col>
                    <Col lg="6">
                      <div className="single-fine-second-col-field-value" />
                    </Col>
                  </Row>
                  <Row>
                    <Col lg="6">
                      <div className="single-fine-second-col-field-label">
                        Penalty Points Amount :
                      </div>
                    </Col>
                    <Col lg="6">
                      <div className="single-fine-second-col-field-value" />
                    </Col>
                  </Row>
                  <Row />
                </div>
                <Button className="fine-driver-details-button">
                  VIEW DETAILS
                </Button>
              </Col>
            </Row>
            <Card className="single-fine-offence-card">
                <CardHeader className="single-fine-offence-card-header">
                  <Row>
                    {" "}
                    <img
                      src={require("../../../assets/img/fine-icon.png")}
                      srcSet={`${require("../../../assets/img/fine-icon@2x.png")} 2x, ${require("../../../assets/img/fine-icon@3x.png")} 3x`}
                      alt="fine-icon"
                      className="single-fine-offence-card-icon"
                    />
                    <p className="single-fine-offence-card-title">Offences</p>
                  </Row>
                </CardHeader>
                <CardBody>
                  
                </CardBody>
                <CardFooter>
                  
                </CardFooter>
              </Card>
          </Container>
        </div>
      </div>
    );
  }
}

export default FineSearchSingle;
