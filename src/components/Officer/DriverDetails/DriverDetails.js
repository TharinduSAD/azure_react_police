import React, { Component } from "react";
import { Row, Container, Col, Button,Table,FormGroup } from "reactstrap";
import "./DriverDetails.css";
import {Link} from 'react-router-dom'

class DriverDetails extends Component {
  render() {
    return (
      <div>
        <div className="driver-name">DILSHAN GAMAGE</div>
        <div className="driver-details-container">
          <Container>
            <Row>
              <Col>
                <div className="driver-details-label">Driver ID:</div>
              </Col>
              <Col>
                <div className="driver-details-value">Dilshan</div>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="driver-details-label">First Name:</div>
              </Col>
              <Col>
                <div className="driver-details-value">Gamage</div>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="driver-details-label">Last Name:</div>
              </Col>
              <Col>
                <div className="driver-details-value">NIC</div>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="driver-details-label">Penalty Points:</div>
              </Col>
              <Col>
                <div className="driver-details-value">24</div>
              </Col>
            </Row>

            <hr className="driver-details-hr-line" />
            <Row>
              <Col>
                <div className="driver-details-label">No Of Offenses:</div>
              </Col>
              <Col>
                {" "}
                <div className="driver-details-value">12</div>
              </Col>
            </Row>
            <Table striped className="fine-table" responsive>
            <thead className="fine-table-header">
              <tr>
                <th style={{ textAlign: "center" }}>Fine ID</th>
                <th style={{ textAlign: "center" }}>Driver ID</th>
                <th style={{ textAlign: "center" }}>Location</th>
                <th style={{ textAlign: "center" }}>Date</th>
                <th style={{ textAlign: "center" }}>Vehicle No</th>
                <th style={{ textAlign: "center" }}>Status</th>
                <th style={{ textAlign: "center" }}>View</th>
              </tr>
            </thead>
            <tbody>
              <tr className="fine-table-search-row" id="search-row">
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                        disabled
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                        disabled
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
                <td style={{ textAlign: "center" }}>
                  <FormGroup>
                    <div class="form-group has-input">
                      <input
                        type="text"
                        className="from-control input-search"
                        disabled
                      />
                      <img
                        src={require("../../../assets/img/search.png")}
                        className="search-icon"
                      />
                    </div>
                  </FormGroup>
                </td>
              </tr>
              <tr>
                <th>1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>
                  <Link to="/officer/singleFine">
                    <i class="fa fa-eye view-icon" />
                  </Link>
                </td>
              </tr>
              <tr>
                <th>2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>
                  <i class="fa fa-eye view-icon" />
                </td>
              </tr>
              <tr>
                <th>3</th>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>
                  <i class="fa fa-eye view-icon" />
                </td>
              </tr>
            </tbody>
          </Table>
          </Container>
        </div>
      </div>
    );
  }
}

export default DriverDetails;
