import { createLogic } from "redux-logic";

import { loginTypes } from "../actions";
import { loginActions } from "../actions";

import * as API from "./HTTPClient";
import * as endPoints from "./EndPoints";


export default [
  createLogic({
    type: loginTypes.LOGIN, // only apply this logic to this type
    latest: true, // only take latest
    debounce: 1000, // Wait 1 s before triggering another call

    process({ MockHTTPClient, getState, action }, dispatch, done) {
      let HTTPClient;

      if (MockHTTPClient) {
        HTTPClient = MockHTTPClient;
      } else {
        HTTPClient = API;
      }

      let loginRequestObject = {
        username: action.payload.username,
        pass: action.payload.pass
      };

      HTTPClient.Post(endPoints.LOGIN, loginRequestObject)
        .then(resp => resp.data)
        .then(data => {
          console.log(data);
          localStorage.setItem("jwt", data.token);
          localStorage.setItem("user", JSON.stringify(data.user));
          HTTPClient.setAuth(); // Set token for subsequent calls
          return data.token;
        })
        .then(token => dispatch(loginActions.loginSuccess(token)))
        .catch(err => {
          var errorMessage = "Invalid username or password.";

          console.error("Login Error ", err); // log since could be render err
          if (err && err.code === "ECONNABORTED") {
            errorMessage = "Please check your internet connection.";
          }

          dispatch(
            loginActions.loginFailed({ title: "Error!", message: errorMessage })
          );
        })
        .then(() => done()); // call done when finished dispatching
    }
  })
];
