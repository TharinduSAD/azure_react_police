
export const LOGIN = "/user/login/"
export const ADD_ADMIN ="/user/enrollAdmin"
export const GET_ADMINISTRATORS ="/user/administrators"
export const GET_USER_BY_USERNAME ="/user/getuserbyusername"
export const GET_ADMIN_BY_IDENTITYKEY ="/user/getadminbyuseridentitykey"
export const GET_ADMIN_DETAILS ="/user/getNthAdminOfficer"
export const GET_OFFICER_BY_IDENTITYKEY = "/user/getofficerbyidentitykey"
export const ADD_OFFICER ="/user/addUser"