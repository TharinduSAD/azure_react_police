import axios from "axios";

// TODO: Replace this with actual JWT token from Keycloak
axios.defaults.headers.post["Content-Type"] = "application/json";

// Create axios instance for api calls
var instance = null;

export const setAuth = () => {
  instance = axios.create({
    baseURL: "",
    timeout: 150000,
    headers: {
      Authorization: "Bearer " + localStorage.jwt,
      "Content-Type": "application/json"
    }
  });
  instance.interceptors.response.use(
    function(response) {
      return response;
    },
    function(error) {
      if (error.response.status) {
        if (error.response.status === 401) {
          localStorage.removeItem("jwt");
          localStorage.removeItem("user");
          window.location = "/";
        } else {
          return Promise.reject(error);
        }
      }
    }
  );
};

export const Get = (route, data) => {
  instance || setAuth();
  return instance.get(
    route,
    data == null ? { data: {} } : { data: JSON.stringify(data) }
  );
};

export const Post = (route, data) => {
  instance || setAuth();
  return instance.post(route, JSON.stringify(data));
};

export const Put = (route, data) => {
  debugger;
  instance || setAuth();
  return instance.put(route, JSON.stringify(data));
};

export const AddAdmin = (route, data) => {};
