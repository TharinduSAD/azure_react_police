import { createLogic } from "redux-logic";

import { userTypes } from "../actions";
import { userActions } from "../actions";

import * as API from "./HTTPClient";
import * as endPoints from "./EndPoints";

export default [
  createLogic({
    type: userTypes.ADD_ADMIN,
    latest: true, // only take latest
    debounce: 1000, // Wait 1 s before triggering another call

    process({ MockHTTPClient, getState, action }, dispatch, done) {
      let HTTPClient;

      if (MockHTTPClient) {
        HTTPClient = MockHTTPClient;
      } else {
        HTTPClient = API;
      }

      let adminObject = {
        identity: action.payload.identity,
        identity_key: action.payload.identity_key,
        first_name: action.payload.first_name,
        last_name: action.payload.last_name,
        organization: action.payload.organization,
        admin_level: action.payload.admin_level,
        username: action.payload.username,
        password: action.payload.password,
        station: action.payload.station
      };
      HTTPClient.Post(endPoints.ADD_ADMIN, adminObject)
        .then(resp => resp.data)
        .then(data => {
          return data.msg;
        })
        .then(msg => dispatch(userActions.addAdminSuccess(msg)))
        .catch(err => {
          var errorMessage = "Something went wrong";

          console.error(" Error ", err); // log since could be render err
          if (err && err.code === "ECONNABORTED") {
            errorMessage = "Please check your internet connection.";
          }

          dispatch(
            userActions.addAdminFailed({
              title: "Error!",
              message: errorMessage
            })
          );
        })
        .then(() => done()); // call done when finished dispatching
    }
  }),
  createLogic({
    type: userTypes.GET_ADMINISTRATORS,
    latest: true, // only take latest
    debounce: 1000,

    process({ MockHTTPClient, getState, action }, dispatch, done) {
      let HTTPClient;

      if (MockHTTPClient) {
        HTTPClient = MockHTTPClient;
      } else {
        HTTPClient = API;
      }

      HTTPClient.Get(endPoints.GET_ADMINISTRATORS)
        .then(resp => resp.data)
        .then(data => {
          return data.users;
        })
        .then(msg => dispatch(userActions.getAdministratorsSuccess(msg)))
        .catch(err => {
          var errorMessage = "Something went wrong";

          console.error(" Error ", err); // log since could be render err
          if (err && err.code === "ECONNABORTED") {
            errorMessage = "Please check your internet connection.";
          }

          dispatch(
            userActions.getAdministratorsFailed({
              title: "Error!",
              message: errorMessage
            })
          );
        })
        .then(() => done()); // call done when finished dispatching
    }
  }),
  createLogic({
    type: userTypes.GET_USER_BY_USERNAME,
    latest: true, // only take latest
    debounce: 1000,

    process({ MockHTTPClient, getState, action }, dispatch, done) {
      let HTTPClient;

      if (MockHTTPClient) {
        HTTPClient = MockHTTPClient;
      } else {
        HTTPClient = API;
      }

      let userObject = {
        username: action.payload.username
      };

      console.log(userObject);

      HTTPClient.Post(endPoints.GET_USER_BY_USERNAME, userObject)
        .then(resp => resp.data)
        .then(data => {
          return data.user;
        })
        .then(msg => dispatch(userActions.getUserByUserNameSuccess(msg)))
        .catch(err => {
          var errorMessage = "Something went wrong";

          console.error(" Error ", err); // log since could be render err
          if (err && err.code === "ECONNABORTED") {
            errorMessage = "Please check your internet connection.";
          }

          dispatch(
            userActions.getUserByUserNameFailed({
              title: "Error!",
              message: errorMessage
            })
          );
        })
        .then(() => done()); // call done when finished dispatching
    }
  }),
  createLogic({
    type: userTypes.GET_ADMIN_BY_IDENTITYKEY,
    latest: true, // only take latest
    debounce: 1000,

    process({ MockHTTPClient, getState, action }, dispatch, done) {
      let HTTPClient;

      if (MockHTTPClient) {
        HTTPClient = MockHTTPClient;
      } else {
        HTTPClient = API;
      }

      let userObject = {
        identity: action.payload.identity
      };

      console.log(userObject);

      HTTPClient.Post(endPoints.GET_ADMIN_BY_IDENTITYKEY, userObject)
        .then(resp => resp.data)
        .then(data => {
          return data.user;
        })
        .then(msg => dispatch(userActions.getAdminByIdentityKeySuccess(msg)))
        .catch(err => {
          var errorMessage = "Something went wrong";

          console.error(" Error ", err); // log since could be render err
          if (err && err.code === "ECONNABORTED") {
            errorMessage = "Please check your internet connection.";
          }

          dispatch(
            userActions.getAdminByIdentityKeyFailed({
              title: "Error!",
              message: errorMessage
            })
          );
        })
        .then(() => done());
    }
  }),
  createLogic({
    type: userTypes.GET_ADMIN_DETAILS,
    latest: true, // only take latest
    debounce: 1000,

    process({ MockHTTPClient, getState, action }, dispatch, done) {
      let HTTPClient;

      if (MockHTTPClient) {
        HTTPClient = MockHTTPClient;
      } else {
        HTTPClient = API;
      }

      let userObject = {
        identity: action.payload.identity
      };


      HTTPClient.Post(endPoints.GET_ADMIN_DETAILS, userObject)
        .then(resp => resp.data)
        .then(data => {
          console.log(data);
          return data.msg;
        })
        .then(msg => dispatch(userActions.getAdminDetailsSuccess(msg)))
        .catch(err => {
          var errorMessage = "Something went wrong";

          console.error(" Error ", err); // log since could be render err
          if (err && err.code === "ECONNABORTED") {
            errorMessage = "Please check your internet connection.";
          }

          dispatch(
            userActions.getAdminDetailsFailed({
              title: "Error!",
              message: errorMessage
            })
          );
        })
        .then(() => done());
    }
  }),
  createLogic({
    type: userTypes.GET_OFFICER_BY_IDENTITYKEY,
    latest: true, // only take latest
    debounce: 1000,

    process({ MockHTTPClient, getState, action }, dispatch, done) {
      let HTTPClient;

      if (MockHTTPClient) {
        HTTPClient = MockHTTPClient;
      } else {
        HTTPClient = API;
      }

      let userObject = {
        identity: action.payload.identity
      };

      console.log(userObject);

      HTTPClient.Post(endPoints.GET_OFFICER_BY_IDENTITYKEY, userObject)
        .then(resp => resp.data)
        .then(data => {
          console.log(data.user)
          return data.user;
        })
        .then(msg => dispatch(userActions.getOfficerByIdentityKeySuccess(msg)))
        .catch(err => {
          var errorMessage = "Something went wrong";

          console.error(" Error ", err); // log since could be render err
          if (err && err.code === "ECONNABORTED") {
            errorMessage = "Please check your internet connection.";
          }

          dispatch(
            userActions.getOfficerByIdentityKeyFailed({
              title: "Error!",
              message: errorMessage
            })
          );
        })
        .then(() => done());
    }
  }),
  createLogic({
    type: userTypes.ADD_OFFICER,
    latest: true, // only take latest
    debounce: 1000, // Wait 1 s before triggering another call

    process({ MockHTTPClient, getState, action }, dispatch, done) {
      let HTTPClient;

      if (MockHTTPClient) {
        HTTPClient = MockHTTPClient;
      } else {
        HTTPClient = API;
      }

      const logggedUser =JSON.parse(localStorage.user);
      
      let userObject = {
        identity: action.payload.identity,
        identity_key: action.payload.identity_key,
        firstname: action.payload.first_name,
        lastname: action.payload.last_name,
        organization: action.payload.organization,
        username: action.payload.username,
        password: action.payload.password,
        station: action.payload.station,
        user:logggedUser.identity_key
      };
      
      HTTPClient.Post(endPoints.ADD_OFFICER, userObject)
        .then(resp => resp.data)
        .then(data => {
          return data.msg;
        })
        .then(msg => dispatch(userActions.addOfficerSuccess(msg)))
        .catch(err => {
          var errorMessage = "Something went wrong";

          console.error(" Error ", err); // log since could be render err
          if (err && err.code === "ECONNABORTED") {
            errorMessage = "Please check your internet connection.";
          }

          dispatch(
            userActions.addOfficerFailed({
              title: "Error!",
              message: errorMessage
            })
          );
        })
        .then(() => done()); // call done when finished dispatching
    }
  })
];
