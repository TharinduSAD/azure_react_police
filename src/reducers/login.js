import { loginTypes as types } from "../actions";
import { handleActions } from "redux-actions";

const initialState = {
  username: "",
  hasError: false,
  error: {},
  resp: { status: 0, message: "" },
  loggedIn: false
};

export default handleActions(
  {
    [types.LOGIN]: (state, { payload }) => ({
      ...state,
      username: payload.username,
      hasError: false,
      error: {}
    }),
    [types.LOGIN_SUCCESS]: (state, { payload }) => ({
      ...state,
      username: "",
      hasError: false,
      resp: payload,
      error: null,
      loggedIn: true
    })
  },
  initialState
);
