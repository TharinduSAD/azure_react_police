import { userTypes as types } from "../actions";
import { handleActions } from "redux-actions";

const initialState = {
  officer: "",
  user: "",
  userIdentityKey: "",
  hasError: false,
  identity: "",
  error: {},
  administrators: {},
  administrator:[],
  resp: { status: 0, message: "" }
};

export default handleActions(
  {
    [types.ADD_ADMIN]: (state, { payload }) => ({
      ...state,
      identity: payload.identity,
      hasError: false,
      error: {}
    }),
    [types.ADD_ADMIN_SUCCESS]: (state, { payload }) => ({
      ...state,
      hasError: false,
      error: "",
      resp: payload,
      officer: payload
    }),
    [types.ADD_ADMIN_FAILED]: (state, { payload }) => ({
      ...state,
      hasError: true,
      error: payload,
      resp: "",
      officer: ""
    }),
    [types.GET_ADMINISTRATORS]: (state, payload) => ({
      ...state,
      hasError: false,
      error: {}
    }),
    [types.GET_ADMINISTRATORS_SUCCESS]: (state, { payload }) => ({
      ...state,
      hasError: false,
      error: "",
      resp: payload,
      administrators: payload
    }),
    [types.GET_ADMINISTRATORS_FAILED]: (state, { payload }) => ({
      ...state,
      hasError: true,
      error: "",
      resp: payload
    }),
    [types.GET_USER_BY_USERNAME]: (state, { payload }) => ({
      ...state,
      hasError: false,
      error: ""
    }),
    [types.GET_USER_BY_USERNAME_SUCCESS]: (state, { payload }) => ({
      ...state,
      hasError: false,
      user: payload,
      error: ""
    }),
    [types.GET_USER_BY_USERNAME_FAILED]: (state, { payload }) => ({
      ...state,
      hasError: true,
      error: payload,
      user: ""
    }),
    [types.GET_ADMIN_BY_IDENTITYKEY]: (state, { payload }) => ({
      ...state,
      hasError: false,
      error: ""
    }),
    [types.GET_ADMIN_BY_IDENTITYKEY_SUCCESS]: (state, { payload }) => ({
      ...state,
      hasError: false,
      userIdentityKey: payload,
      error: ""
    }),
    [types.GET_ADMIN_BY_IDENTITYKEY_FAILD]: (state, { payload }) => ({
      ...state,
      hasError: true,
      error: payload,
      userIdentityKey: ""
    }),
    [types.GET_ADMIN_DETAILS]: (state, {payload}) =>({
      ...state,
      hasError: false,
      error: {}
    }),
    [types.GET_ADMIN_DETAILS_SUCCESS]: (state, { payload }) => ({
      ...state,
      hasError: false,
      error: "",
      resp: payload,
      administrator: payload
    }),
    [types.GET_ADMIN_DETAILS_FAILED]: (state, { payload }) => ({
      ...state,
      hasError: true,
      error: "",
      resp: payload
    }),
    [types.GET_ADMIN_BY_IDENTITYKEY]: (state, { payload }) => ({
      ...state,
      hasError: false,
      error: ""
    }),
    [types.GET_OFFICER_BY_IDENTITYKEY_SUCCESS]: (state, { payload }) => ({
      ...state,
      hasError: false,
      userIdentityKey: payload,
      error: ""
    }),
    [types.GET_OFFICER_BY_IDENTITYKEY_FAILED]: (state, { payload }) => ({
      ...state,
      hasError: true,
      error: payload,
      userIdentityKey: ""
    }),
    [types.ADD_OFFICER]: (state, { payload }) => ({
      ...state,
      identity: payload.identity,
      hasError: false,
      error: {}
    }),
    [types.ADD_OFFICER_SUCCESS]: (state, { payload }) => ({
      ...state,
      hasError: false,
      error: "",
      resp: payload,
      officer: payload
    }),
    [types.ADD_OFFICER_FAILED]: (state, { payload }) => ({
      ...state,
      hasError: true,
      error: payload,
      resp: "",
      officer: ""
    })
  },
  initialState
);
