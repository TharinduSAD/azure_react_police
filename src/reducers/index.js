import { combineReducers } from "redux";
import LoginReducer from "./login";
import UserReducer from './user'


const rootReducer = combineReducers({
  login: LoginReducer,
  user: UserReducer
});

export default rootReducer;
