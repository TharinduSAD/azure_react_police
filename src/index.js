import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import SignIn from "./components/SignIn/SignIn";
import AdminLayout from "./components/Admin/AdminLayout/AdminLayout";
import OfficerLayout from "./components/Officer/OfficerLayout/OfficerLayout";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import { createLogicMiddleware } from "redux-logic";
import { createStore, applyMiddleware, compose } from "redux";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";

import reducers from "./reducers";
import services from "./services";

const logicMiddleware = createLogicMiddleware(services, {});

const middleware = applyMiddleware(logicMiddleware);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(middleware);

let store = createStore(reducers, enhancer);

// let store = applyMiddleware(promiseMiddeleware)(createStore);

ReactDOM.render(
  // <Provider
  //   store={store(
  //     reducers,
  //     window.__REDUX_DEVTOOLS_EXTENSION__ &&
  //       window.__REDUX_DEVTOOLS_EXTENSION__()
  //   )}
  // >
  <Provider store={store}>
    
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/signin" component={SignIn} />
        <Route
          path="/admin"
          name="Admin"
          render={props => <AdminLayout {...props} />}
        />
        <Route
          path="/officer"
          name="Officer"
          render={props => <OfficerLayout {...props} />}
        />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
