import { createAction } from "redux-actions";

// Types
export const LOGIN = "login/LOGIN";
export const LOGIN_FAILED = "login/LOGIN_FAILED";
export const LOGIN_SUCCESS = "login/LOGIN_SUCCESS";

export default {
  doLogin: createAction(LOGIN),
  loginSuccess: createAction(LOGIN_SUCCESS),
  loginFailed: createAction(LOGIN_FAILED)
};


