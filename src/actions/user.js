import { createAction } from "redux-actions";

export const ADD_ADMIN = "user/ADD_ADMIN";
export const ADD_ADMIN_FAILED = "user/ADD_ADMIN_FAILED";
export const ADD_ADMIN_SUCCESS = "user/ADD_ADMIN_SUCCESS";
export const GET_ADMINISTRATORS = "user/GET_ADMINISTRATORS";
export const GET_ADMINISTRATORS_SUCCESS = "user/GET_ADMINISTRATORS_SUCCESS";
export const GET_ADMINISTRATORS_FAILED = "user/GET_ADMINISTRATORS_FAILED";
export const GET_USER_BY_USERNAME = "user/GET_USER_BY_USERNAME";
export const GET_USER_BY_USERNAME_SUCCESS = "user/GET_USER_BY_USERNAME_SUCCESS";
export const GET_USER_BY_USERNAME_FAILED = "user/GET_USER_BY_USERNAME_FAILED";
export const GET_ADMIN_BY_IDENTITYKEY = "user/GET_ADMIN_BY_IDENTITYKEY";
export const GET_ADMIN_BY_IDENTITYKEY_SUCCESS =
  "user/GET_ADMIN_BY_IDENTITYKEY_SUCCESS";
export const GET_ADMIN_BY_IDENTITYKEY_FAILD =
  "user/GET_ADMIN_BY_IDENTITYKEY_FAILD";

export const GET_ADMIN_DETAILS ="user/GET_ADMIN_DETAILS";
export const GET_ADMIN_DETAILS_SUCCESS = "user/GET_ADMIN_DETAILS_SUCCESS";
export const GET_ADMIN_DETAILS_FAILED = "user/GET_ADMIN_DETAILS_FAILED";
export const GET_OFFICER_BY_IDENTITYKEY = "user/GET_OFFICER_BY_IDENTITYKEY";
export const GET_OFFICER_BY_IDENTITYKEY_SUCCESS =
  "user/GET_OFFICER_BY_IDENTITYKEY_SUCCESS";
export const GET_OFFICER_BY_IDENTITYKEY_FAILED =
  "user/GET_OFFICER_BY_IDENTITYKEY_FAILED";

export const ADD_OFFICER = "user/ADD_OFFICER";
export const ADD_OFFICER_FAILED = "user/ADD_OFFICER_FAILED";
export const ADD_OFFICER_SUCCESS = "user/ADD_OFFICER_SUCCESS";



export default {
  addAdmin: createAction(ADD_ADMIN),
  addAdminSuccess: createAction(ADD_ADMIN_SUCCESS),
  addAdminFailed: createAction(ADD_ADMIN_FAILED),
  getAdministrators: createAction(GET_ADMINISTRATORS),
  getAdministratorsSuccess: createAction(GET_ADMINISTRATORS_SUCCESS),
  getAdministratorsFailed: createAction(GET_ADMINISTRATORS_FAILED),
  getUserByUserName: createAction(GET_USER_BY_USERNAME),
  getUserByUserNameSuccess: createAction(GET_USER_BY_USERNAME_SUCCESS),
  getUserByUserNameFailed: createAction(GET_USER_BY_USERNAME_FAILED),
  getAdminByIdentityKey: createAction(GET_ADMIN_BY_IDENTITYKEY),
  getAdminByIdentityKeySuccess: createAction(GET_ADMIN_BY_IDENTITYKEY_SUCCESS),
  getAdminByIdentityKeyFailed: createAction(GET_ADMIN_BY_IDENTITYKEY_FAILD),
  getAdminDetails: createAction(GET_ADMIN_DETAILS),
  getAdminDetailsSuccess: createAction(GET_ADMIN_DETAILS_SUCCESS),
  getAdminDetailsFailed: createAction(GET_ADMIN_DETAILS_FAILED),
  getOfficerByIdentityKey: createAction(GET_OFFICER_BY_IDENTITYKEY),
  getOfficerByIdentityKeySuccess: createAction(GET_OFFICER_BY_IDENTITYKEY_SUCCESS),
  getOfficerByIdentityKeyFailed: createAction(GET_OFFICER_BY_IDENTITYKEY_FAILED),
  addOfficer: createAction(ADD_OFFICER),
  addOfficerSuccess: createAction(ADD_OFFICER_SUCCESS),
  addOfficerFailed: createAction(ADD_OFFICER_FAILED)
};
